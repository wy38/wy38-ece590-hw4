//
//  drawingView.swift
//  Homework 4
//
//  Created by Yuan on 2/16/17.
//  Copyright © 2017 yuanwanxin. All rights reserved.
//

import UIKit

class drawingView: UIView {

    let size: CGFloat = 60
    let lineWidth: CGFloat = 2
    var fillColor: UIColor!
    var path: UIBezierPath!
    
    func randomColor() -> UIColor {
        let hue:CGFloat = CGFloat(Float(arc4random()) / Float(UINT32_MAX))
        return UIColor(hue: hue, saturation: 0.8, brightness: 1.0, alpha: 0.8)
    }
    
    func pointFrom(_ angle: CGFloat, radius: CGFloat, offset: CGPoint) -> CGPoint {
        return CGPoint(x: radius * cos(angle) + offset.x, y: radius * sin(angle) + offset.y)
    }
    
    // Function to draw stars
    func starPathInRect(_ rect: CGRect) -> UIBezierPath {
        let path = UIBezierPath()
        let starExtrusion:CGFloat = 15.0
        let center = CGPoint(x: rect.width / 2.0, y: rect.height / 2.0)
        let pointsOnStar = 5
        var angle:CGFloat = -CGFloat(M_PI / 2.0)
        let angleIncrement = CGFloat(M_PI * 2.0 / Double(pointsOnStar))
        let radius = rect.width / 2.0
        var firstPoint = true
        for _ in 1...pointsOnStar {
            
            let point = pointFrom(angle, radius: radius, offset: center)
            let nextPoint = pointFrom(angle + angleIncrement, radius: radius, offset: center)
            let midPoint = pointFrom(angle + angleIncrement / 2.0, radius: starExtrusion, offset: center)
            
            if firstPoint {
                firstPoint = false
                path.move(to: point)
            }
            
            path.addLine(to: midPoint)
            path.addLine(to: nextPoint)
            
            angle += angleIncrement
        }
        
        path.close()
        return path
    }
    
    
    func randomPath() -> UIBezierPath {
        
        let insetRect = self.bounds.insetBy(dx: lineWidth,dy: lineWidth)
        return starPathInRect(insetRect)
        
    }
    
    init(origin: CGPoint) {
        
        super.init(frame: CGRect(x: 0.0, y: 0.0, width: size, height: size))
        self.fillColor = randomColor()
        self.path = randomPath()
        self.center = origin
        self.backgroundColor = UIColor.clear
        initGestureRecognizers()
        
    }
    
    func initGestureRecognizers() {
        let panGR = UIPanGestureRecognizer(target: self, action: #selector(drawingView.didPan(_:)))
        addGestureRecognizer(panGR)
    }
    
    func didPan(_ panGR: UIPanGestureRecognizer) {
        
        self.superview!.bringSubview(toFront: self)
        var translation = panGR.translation(in: self)
        translation = translation.applying(self.transform)
        self.center.x += translation.x
        self.center.y += translation.y
        panGR.setTranslation(CGPoint.zero, in: self)
    }
    
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func draw(_ rect: CGRect) {
        
        self.fillColor.setFill()
        self.path.fill()
        UIColor.black.setStroke()
        path.lineWidth = self.lineWidth
        path.stroke()
        
    }
}
