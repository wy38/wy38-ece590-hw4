Basic Functions:

1. Realize the function to flip and flip back only on the Info page about me.

2. As "Starry night" painted by Van Gogh is my favourite drawing and I like drawing pictures and stars, I choose "star" as the topic for my drawing. 

(1) Use an UIImage to display Van Gogh's drawing of starry night

(2) Use Graphic Context with vector graphic manipulation to display a black rectangle as the background

(3) Use animation as fade in and fade out to display images

(4) Use shadows for the UIImage and use colors for graphic context.

Extra Features:

1. Use a page controller as the transition

2. Add function to draw stars by tapping the screen, the color of stars is set to be random chosen. Also, the stars can be moved. And there is fade in and fade out animation when drawing the stars.

3. Add audio as background music. (This is a Chinese song which name also means "starry night")
